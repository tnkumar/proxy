from sqlalchemy import func
from proxy.core import db

class Device(db.Model):
    """ Create the device table."""
    id = db.Column(db.Integer, primary_key=True)
    deviceId = db.Column(db.String(25))
    devicePassword = db.Column(db.String(25))
    ip = db.Column(db.String(25))
    lat = db.Column(db.String(25))
    ltd = db.Column(db.String(25))
    version = db.Column(db.String(25))
    checksum = db.Column(db.Text)
    status = db.Column(db.String(25))
    verified = db.Column(db.String(25))
    signature = db.Column(db.Text)
    path = db.Column(db.Text)
    
    def __init__(self, deviceId, devicePassword, ip, lat, ltd, version,
            checksum, status, verified, signature, path):
        self.deviceId = deviceId
        self.devicePassword = devicePassword
        self.ip = ip
        self.lat = lat
        self.ltd = ltd
        self.version = version
        self.checksum = checksum
        self.verified = verified
        self.status = status
        self.signature = signature
        self.path = path

class Event(db.Model):
    """ Create the event log table. """
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=func.now())
    event = db.Column(db.Text)

    def __init__(self, event):
        self.event = event

class Version(db.Model):
    """ Create the version table. """
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=func.now())
    version = db.Column(db.String(25))
    status = db.Column(db.String(25))
    checksum = db.Column(db.Text)
    path = db.Column(db.Text)
    signature = db.Column(db.Text)

    def __init__(self, version, status, checksum, path, signature):
        self.version = version
        self.status = status
        self.checksum = checksum
        self.path = path
        self.signature = signature

class Sim(db.Model):
    """ Create the device."""
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=func.now())
    deviceId = db.Column(db.String(25))
    devicePassword = db.Column(db.String(25))
    ip = db.Column(db.String(25))
    version = db.Column(db.String(25))
    checksum = db.Column(db.Text)
    path = db.Column(db.Text)

    def __init__(self, deviceID, devicePassword, ip, version, checksum,
            path):
        self.deviceId = deviceID
        self.devicePassword = devicePassword
        self.ip = ip
        self.version = version
        self.checksum = checksum
        self.path = path
