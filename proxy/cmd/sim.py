import logging

from proxy.web import sim
from proxy.core.utils import setup_logging

log = logging.getLogger(__name__)

def main():
    setup_logging()

    sim.main()

