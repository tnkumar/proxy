import logging

from proxy.web import app
from proxy.core.utils import setup_logging

log = logging.getLogger(__name__)

def main():
    setup_logging()

    app.main()

