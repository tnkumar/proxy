import csv
import logging
import json
from uuid import getnode as get_mac
from cStringIO import StringIO

from collections import OrderedDict

from flask_googlemaps import Map
from flask import abort
import geocoder

from proxy.db.model import Event, Device, Version
from proxy.core import app
from proxy.core import client
from proxy.core import db
from proxy.core import utils

LOG = logging.getLogger(__name__)

def add_device(data):
    """ Add a device through the Rest API. """
    LOG.info('Adding device to database.')
    geo_data = geocoder.freegeoip(data['ip'])
    if not geo_data:
        LOG.warning('Unable to determine geodata for %s' % data['ip'])
        return False

    try:
        db.create_all()
        device = Device(data['deviceId'], data['devicePassword'], data['ip'],
           geo_data.latlng[0], geo_data.latlng[1], data['version'],
           'Unknown', 'Unknown', 'unverified', 'Unknown', 'Unknown')
        db.session.add(device)
        db.session.commit()
        db.session.close()

        utils.add_event('Add device from %s' % data['ip'])
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        return False

    return True

def list_device():
    """ List all the devices in the database."""
    LOG.info('List devices in database.')
    try:
        db.create_all()
        data = Device.query.all()
        db.session.close()
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        return []

    return [OrderedDict(id=r.id, deviceId=r.deviceId, ip=r.ip, lat=r.lat, ltd=r.ltd,
                version=r.version, checksum=r.checksum, status=r.status,
                verified=r.verified) for r in data]

def fetch_device(device_id):
    """ Get the sepcified device id from the database and check signature."""
    LOG.info('Checking device for %s', device_id)
    try:
        data = Device.query.filter_by(id=device_id).first()
        db.session.close()

        if not data:
            abort(404)

        if data.signature != 'Unknown':
            response = client.sign_device(device_id, data)

            if not response:
                return False

            if response.status_code == 200:
                d = json.loads(response.content)
                if 'verificationResult' in d:
                    a_checksum = d['details']['dataHash']['value']
                    checksum = data.checksum
                    if a_checksum == checksum:
                       Device.query.filter_by(id=device_id).update(
                                {'status': 'PASS', 'verified': 'verified',
                                 'signature': d['signature']})
                       db.session.commit()
                    else:
                        Device.query.filter_by(id=device_id).update(
                            {'status': 'FAIL',
                            'verified': 'unverified'})
                        db.session.commit()
                    utils.add_event('Device checked for %s' % device_id)
                    db.session.close()
        return True
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        return False

def get_device(device_id):
    """ Get the sepcified device id from the database."""
    LOG.info('Fetch device id for %s', device_id)
    try:
        data = Device.query.filter_by(id=device_id).all()
        db.session.close()
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        return []

    return [OrderedDict(id=r.id, deviceId=r.id, ip=r.ip, lat=r.lat, ltd=r.ltd,
            version=r.version, checksum=r.checksum, 
            status=r.status, verified=r.verified) for r in data]

def delete_device(device_id):
    """ Remove the device from the database."""
    try:
        data = Device.query.filter_by(id=device_id).delete()
        db.session.commit()
        db.session.close()
        utils.add_event('Deleted device %s' % device_id)
        return True
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        return False

def get_unverified_devices():
    """ Get all the unverified devices in the database."""
    try:
        data = Device.query.filter_by(verified="unverified").all()
	db.session.close()
        return data
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        return []

def get_verified_devices():
    """ Get all the verified devices in the database."""
    try:
        data = Device.query.filter_by(verified="verified").all()
	db.session.close()
        return data
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        return []

def upload_device(f):
    """ Process the CSV text file to add new devices. """
    try:
        db.create_all()
        file_contents = StringIO(f.stream.read())
        render = csv.DictReader(file_contents)
        checksum = utils.get_checksum()
        for row in render:
            result = {}

            if not row['name']:
                return False

            if not row['devicePassword']:
                return False

            if not checksum:
                return False

            if not row['ip']:
                return False

            if not row['version']:
                return False

            result['deviceId'] = row['name']
            result['devicePassword'] = row['devicePassword']
            result['checksum'] = checksum
            result['ip'] = row['ip']
            result['version'] = row['version']

            d = Version.query.filter_by(version=row['version']).first()
            db.session.close()
            if d:
                result['siganture'] = d['signature']
                if d.status == 'Signed':
                    result['verified'] = 'verified'
                elif d.status == 'Usngined':
                    result['verified'] = 'unverified'
                else:
                    result['verified'] = 'unverified'

            add_device(result)
        return True
    except Exception as e:
        LOG.exception(e)
        return False

def create_map():
    """ Create a google map object to be displayed in the dashboard."""
    try:
        data = Device.query.all()
	db.session.close()
        if not data:
            markers_list = []
        else:
            markers_list = []
            for row in data:
                if row.status == 'PASS':
                    icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                elif row.status == 'FAIL':
                    icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                else:
                    icon = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
                markers_list.append(
                    {'icon': icon,
                    'lat': row.lat,
                    'lng': row.ltd,
                    'infobox': ("<b>IP:</b> {} <br> <b>Checksum:</b> {} ".format(
                        row.ip, row.checksum))})

    except Exception as e:
        LOG.exception('Exception raised: %s', e)
	db.session.close()
        markers_list = []

    return Map(identifier='iotMap',
                varname='iotMap',
                style=(
                  "height:75%;"
                  "width:100%;"),
                lat=43.7712,
                lng=-79.2144,
                markers=markers_list,
                zoom=5)
