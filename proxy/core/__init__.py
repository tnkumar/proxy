import os

from flask import Flask
from flask_sqlalchemy import  SQLAlchemy
from flask_googlemaps import GoogleMaps

from proxy.core import config

app = Flask(__name__, template_folder='templates')
#app.config.from_pyfile('config.py')
app.config['GOOGLEMAPS_KEY'] = os.environ['GOOGLEMAPS_KEY']
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s/%s' \
        % (config.DATABASE_USER, config.DATABASE_PASS, 
            config.DATABASE_HOST,config.DATABASE_DB)
app.config['SQLALCHEMY_POOL_SIZE'] = 100
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['UPLOAD_FOLDER'] = config.UPLOAD_FOLDER
db = SQLAlchemy(app)
GoogleMaps(app)
app.secret_key = 'this is really the key'
