import logging
import requests
import hashlib
import json
import base64

from proxy.core import config

LOG = logging.getLogger(__name__)

HEADERS = {
    'Content-Type': "application/json",
    'Authorization': config.AUTH,
    'Cache-Control': "no-cache",
}

URL = config.ENDPOINT

def sign_device(device_id, data):
    """ Make sure that the signature that we have is valid."""
    LOG.info('Verifying device %s', device_id)

    try:
        url = URL + '/occhub/proxy/dcs/api/v1/signature/details' 
        payload = {'signature': data.signature}
        return requests.post(url, json=payload, headers=HEADERS, verify=False)
    except Exception as e:
        LOG.exception(e)
        return None

def register_device(device_id, data):
    """ Get the siganture of a device with a given checksum."""
    LOG.info('Check device %s', device_id)

    try:
        url = URL + '/occhub/proxy/dcs/api/v1/signature' 

        _hash = base64.b64encode(bytearray.fromhex(data.checksum))
        payload = {"dataHash": 
            {"algorithm": "SHA-256","value": "%s" % _hash},  
            "level": 0}
        return requests.post(url, json=payload, headers=HEADERS, verify=False)
    except Exception as e:
        LOG.exception(e)
        return None

def sign_software(checksum):
    """ Get the signature of a given checksum."""
    LOG.info('Signing software')

    try:
        url = URL + '/occhub/proxy/dcs/api/v1/signature' 
        _hash = base64.b64encode(bytearray.fromhex(checksum))
        payload = {"dataHash": 
            {"algorithm": "SHA-256","value": "%s" % _hash},  
            "level": 0}
        return requests.post(url, json=payload, headers=HEADERS, verify=False)
    except Exception as e:
        LOG.exception(e)
        return None
