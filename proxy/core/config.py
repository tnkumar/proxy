import logging
import os

# Proxy configuration
LOG_FILE = os.getenv('LOG_FILE', None)
LOG_LEVEL = getattr(logging, os.getenv("LOG_LEVEL", "debug").upper())
POLL_INTERVAL = os.getenv('POLL_INTERVAL', 300)

# Blockchain services
ENDPOINT = os.getenv('ENDPOINT', None)
AUTH = os.getenv('AUTH', None)

# Proxy database configuration
DATABASE_HOST = os.getenv('DATABASE', 'db')
DATABASE_USER = os.getenv('DB_USER', 'proxy')
DATABASE_PASS = os.getenv('DB_PASS', 'proxy')
DATABASE_DB = os.getenv('DB', 'proxy')

# Upload folder
UPLOAD_FOLDER = os.getenv('UPLOAD_FOLDER', '/uploads')
