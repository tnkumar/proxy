import logging
import hashlib
import json

from collections import OrderedDict

from flask import abort

from proxy.db.model import Event, Device, Version
from proxy.core import app
from proxy.core import utils
from proxy.core import client
from proxy.core import db

LOG = logging.getLogger(__name__)

def add_version(data):
    """Add version software to database."""
    LOG.info('Adding version')

    try:
        version = Version.query.filter_by(version=data['version']).all()
        if not version:
            db.create_all()

            checksum = utils.get_checksum()

            version = Version(data['version'], 'Unknown', checksum, 'Uknown')
            db.session.add(version)
            db.session.commit()
            db.session.close()
            return True
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return False

def list_versions():
    """ List all the version in the database."""
    LOG.info('List versions in the database.')
    try:
        db.create_all()
        data = Version.query.all()
        db.session.close()
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return []

    return [OrderedDict(id=r.id, version=r.version, status=r.status)
            for r in data]

def get_version(sw_version):
    """ Return all the versions in the version table."""
    LOG.info('Getting verson %s', sw_version)
    try:
        data = Version.query.filter_by(version=sw_version).all()
        db.session.close()
        if not data:
            abort(404)
        return [OrderedDict(id=r.id,  status=r.status)
                for r in data]
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return []

def delete_version(sw_version):
    """ Delete the sofware version from the databse."""
    LOG.info("Deleting version %s", sw_version)
    try:
        Version.query.filter_by(version=sw_version).delete()
        db.session.commit()
        db.session.close()
        utils.add_event('Deleted software version %s' % sw_version)

        Device.query.filter_by(version=sw_version).update(
                {'status': 'Unknown', 'verified': 'unverified', 'checksum':
                 'Unknown', 'signature': 'Unkwnown'})
        db.session.commit()
	db.session.close()
        return True
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return False

def sign_software(sw_version):
    """ Sign the sofware in the database."""
    LOG.info('Signing assoicated sofware: %s', sw_version)
    try:
        data = Version.query.filter_by(version=sw_version).all()
	db.session.close()
        if not data:
            abort(404)
        for row in data:
            response = client.sign_software(row.checksum)
            if not response:
                return False

            if response.status_code == 200:
                d = json.loads(response.content)
                if 'verificationResult' not in d:
                    sign_software(sw_version, d, 'Unsigned')
                elif d['verificationResult']['status'] == 'OK':
                    status = _sign_software(sw_version, d, row.checksum, 'Signed')
                else:
                    status = _sign_software(sw_version, d, row.checksum, 'Unsigned')
                db.session.close()
                return True
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return False

def upload_software(filename, version, block_size=65536):
    """ Upload software """
    try:
        sha256 = hashlib.sha256()
        with open(filename, 'rb') as f:
            for block in iter(lambda: f.read(block_size), b''):
                sha256.update(block)
        checksum = sha256.hexdigest()
        db.create_all()
        version = Version(version, 'Unsigned', checksum, filename, 'Unknown')
        db.session.add(version)
        db.session.commit()
        db.session.close()
        return True
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return False

def _sign_software(sw_version, data, checksum, action):
    """ Update the software versions in the database."""
    try:
        v = Version.query.filter_by(version=sw_version).first()
        db.session.close()

        if not v:
            abort(404)

        if 'verificationResult' in data:
            if data['verificationResult']['status'] == 'OK':
                Version.query.filter_by(version=sw_version).update(
                    {'status': action, 
                     'checksum': data['details']['dataHash']['value'],
                    'signature': data['signature']})
                Device.query.filter_by(version=sw_version).update(
                        {'status': 'PASS', 'verified': 'verified',
                         'checksum': data['details']['dataHash']['value'], 
                         'signature': data['signature']})
                db.session.commit()
            else:
                Version.query.filter_by(version=sw_version).update(
                    {'status': action})
                Device.query.filter_by(version=sw_version).update(
                    {'status': 'FAIL', 'verified': 'unverified'})
                db.session.commit()
        db.session.close()
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()

def list_all_versions():
    """ List all the versions in the database."""
    LOG.info('List unsigned versions of software')
    try:
        db.create_all()
        versions = []
        for version in  Version.query.distinct(Version.version):
            versions.append(version)
	db.session.close()
        return versions
    except Exception as e:
        LOG.exception(e)
        db.session.rollback()
        db.session.close()
        return []
