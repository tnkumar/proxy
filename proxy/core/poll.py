import base64
import requests
import logging
import pprint
import json

from proxy.db.model import Device,Version
from proxy.core import client
from proxy.core import utils
from proxy.core import db

LOG = logging.getLogger(__name__)

def poll_device():
    """ Check all unverified devices and register them."""
    LOG.info('Polling devices')

    try:
        data = Version.query.filter_by(status='Signed').all()
        db.session.close()

        if not data:
            return

        for row in data:
            LOG.info('Checking version %s', row.version)
    except Exception as e:
        LOG.exception(e)
        db.rollback()
        db.session.close()
