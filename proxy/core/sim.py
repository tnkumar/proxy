import logging
import json
import hashlib
import os

from flask import abort
import geocoder

from werkzeug.utils import secure_filename

from proxy.db.model import Sim
from proxy.core import app
from proxy.core import client
from proxy.core import db
from proxy.core import utils

LOG = logging.getLogger(__name__)

def upload_device(request):
    """ Process the CSV text file to add new devices. """
    try:
        db.create_all()

        file = request.files['file']
        if file:
            version = request.form['version']
            filename = secure_filename(file.filename)
            file_dir = os.path.join(app.config['UPLOAD_FOLDER'], version)
            if not os.path.exists(file_dir):
                os.makedirs(file_dir)

            file.save(os.path.join(file_dir, filename))

            sim = Sim(request.form['name'],
                      request.form['password'],
                      request.form['ip'],
                      request.form['version'],
                      determine_checksum(os.path.join(file_dir, file.filename)),
                      file_dir)
            db.session.add(sim)
            db.session.commit()
            db.session.close()
        return True
    except Exception as e:
        LOG.exception(e)
        return False

def list_devices():
    try:
        db.create_all()
        sim = Sim.query.all()
        db.session.close()
        return sim
    except Exception as e:
        LOG.exception('Exception raised: %s', e)
        db.session.rollback()
        db.session.close()
        return []

def determine_checksum(f, block_size=65536):
    sha256 = hashlib.sha256()
    with open(f, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()
