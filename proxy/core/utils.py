import logging
import sys
import hashlib
import uuid

from proxy.core import config
from proxy.core import db
from proxy.db.model import Event

logger = logging.getLogger('proxy')


def setup_logging():
    """Logging setup."""
    logger = logging.getLogger('proxy')

    formatter = logging.Formatter(('%(asctime)s '
                    '- %(name)s - %(levelname)s - %(message)s'))
    handler = None
    if config.LOG_FILE:
        handler = logging.FileHandler(config.LOG_FILE)
    else:
        handler = logging.StreamHandler(sys.stdout)

    logger.setLevel(config.LOG_LEVEL)
    handler.setLevel(config.LOG_LEVEL)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

def add_event(msg):
    """ Add the event message to the event table."""
    try:
        event = Event(msg)
        db.session.add(event)
        db.session.close()
    except Exception as e:
        logger.exception('Exception raised: %s', e)
        db.session.rollback()

def get_all_events():
    """ List all the events in the event table."""
    try:
        data = Event.query.all()
        db.session.close()
        return data
    except Exception as e:
        logger.exception('Exception raised: %s', e)
        db.session.rollback()
        return []

def get_checksum():
    """ Generate a checksum for a random string."""
    checksum = uuid.uuid4().hex
    return hashlib.sha256(checksum.encode()).hexdigest()
