import atexit
import os
import logging
import sys

from gevent.pywsgi import WSGIServer

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger

from flask_sqlalchemy import  SQLAlchemy
from flask import Flask, jsonify, abort
from flask import render_template
from flask import request

from werkzeug.utils import secure_filename

from proxy.core import device
from proxy.core import config
from proxy.core import sim
from proxy.core import version as v
from proxy.core import utils

LOG = logging.getLogger(__name__)

app = Flask(__name__, template_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s/%s' \
        % (config.DATABASE_USER, config.DATABASE_PASS,
            config.DATABASE_HOST,config.DATABASE_DB)
app.config['SQLALCHEMY_POOL_RECYCLE'] = 599
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 20
app.config['UPLOAD_FOLDER'] = config.UPLOAD_FOLDER
db = SQLAlchemy(app)
app.secret_key = 'this is really the key'

scheduler = BackgroundScheduler()

@app.route('/')
def index():
    """ Load the index page."""
    return render_template('upload.html')

@app.route('/upload', methods=['POST'])
def upload():
    d = sim.upload_device(request)
    return render_template('sim.html', data=d, d=s)

def main():
    """ Poll the device table interval every XX length of time."""
    """ Load the proxy application."""
    http_server = WSGIServer(('', 8080), app)
    http_server.serve_forever()
