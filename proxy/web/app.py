import atexit
import os
import logging
import sys

from gevent.pywsgi import WSGIServer

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger

from flask_googlemaps import GoogleMaps
from flask_sqlalchemy import  SQLAlchemy
from flask import Flask, jsonify, abort
from flask import render_template
from flask import request

from werkzeug.utils import secure_filename

from proxy.core import device
from proxy.core import config
from proxy.core.poll import poll_device
from proxy.core import version as v
from proxy.core import utils

LOG = logging.getLogger(__name__)

app = Flask(__name__, template_folder='templates')
app.config['GOOGLEMAPS_KEY'] = os.environ['GOOGLEMAPS_KEY']
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s/%s' \
        % (config.DATABASE_USER, config.DATABASE_PASS,
            config.DATABASE_HOST,config.DATABASE_DB)
app.config['SQLALCHEMY_POOL_RECYCLE'] = 599
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 20
app.config['UPLOAD_FOLDER'] = config.UPLOAD_FOLDER
db = SQLAlchemy(app)
app.secret_key = 'this is really the key'
GoogleMaps(app)

POLL_INTERVAL = config.POLL_INTERVAL

scheduler = BackgroundScheduler()

@app.route('/')
def index():
    """ Load the index page."""
    return render_template('index.html')

@app.route('/dashboard')
def dashboard():
    """ Load the google maps dashboard."""
    gMap = device.create_map()
    if gMap:
        return render_template('dashboard.html', map=gMap)

@app.route('/devices', methods=['GET', 'POST'])
def devices():
    """ Load the devices page."""
    vData = device.get_verified_devices()
    uData = device.get_unverified_devices()
    return render_template('devices.html', v=vData, u=uData)

@app.route('/versions')
def versions():
    """" Load the sofware versions page."""
    d = v.list_all_versions()
    return render_template('versions.html', data=d)

@app.route('/register', methods=['POST'])
def register():
    """ Upload the devices from csv."""
    f = request.files['file']
    d = device.upload_device(f)
    return render_template('client.html', data=d)

@app.route('/upload', methods=['POST'])
def upload():
    """ Upload the sofware version and grab a checksum."""
    file = request.files['file']
    version = request.form['version']
    if file:
        filename = secure_filename(file.filename)
        file_dir = os.path.join(app.config['UPLOAD_FOLDER'], version)
        if not os.path.exists(file_dir):
            os.makedirs(file_dir)
        
        file.save(os.path.join(file_dir, filename))
        d = v.upload_software(
            os.path.join(file_dir, filename), version)
    return render_template('client.html', data=d)

@app.route('/events')
def events():
    """ Load the events page."""
    data = utils.get_all_events()
    if not data:
        data = []
    return render_template('events.html', data=data)

# Rest API
@app.route('/v1/device', methods=['POST'])
def add():
    """ Add a device through the rest api. """
    data = request.get_json()
    if not data or 'ip' not in data:
        abort(404)
    return jsonify({'status': device.add_device(data)})

@app.route('/v1/device')
def list_device():
    """ List all the devices in the database."""
    return jsonify({'devices': device.list_device()})

@app.route('/v1/device/<string:device_id>', methods=["GET"])
def fetch_device(device_id):
    """ Fetch the device id from the database."""
    return jsonify({'devices': device.get_device(device_id)})

@app.route('/v1/device/<string:device_id>', methods=["POST"])
def register_device(device_id):
    """" Register the device. """
    return jsonify({'status': device.fetch_device(device_id)})

@app.route('/v1/device/<string:device_id>', methods=["DELETE"])
def delete_device(device_id):
    """ Delete the device from the database."""
    return jsonify({'status': device.delete_device(device_id)})

@app.route('/v1/version')
def list():
    """ List all the software versions in the database."""
    return jsonify({'version': v.list_versions()})

@app.route('/v1/version/<string:sw_version>')
def get(sw_version):
    """ Get information for the sofware version through the rest api."""
    return jsonify({'version': v.get_version(sw_version)})

@app.route('/v1/version/<string:sw_version>', methods=["DELETE"])
def remove_version(sw_version):
    """ Remove the sofware version from the rest api."""
    return jsonify({'status': v.delete_version(sw_version)})

@app.route('/v1/version/<string:sw_version>', methods=["POST"])
def sign(sw_version):
    """ Sign a given software version."""
    return jsonify({'status': v.sign_software(sw_version)})

@app.teardown_appcontext
def shutdown_session(exception=None):
    """ Terminate the session when ready."""
    db.session.remove()

def main():
    """ Poll the device table interval every XX length of time."""
    scheduler.add_job(poll_device, trigger='interval',
            seconds=int(POLL_INTERVAL))
    scheduler.start()

    """ Load the proxy application."""
    try:
        http_server = WSGIServer(('', 80), app)
        http_server.serve_forever()
    except:
        scheduler.shutdown()
