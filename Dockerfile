from ubuntu:xenial

run set -e && \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y git python python-pip libmysqlclient-dev tzdata && \
    pip install -U pip

run mkdir -p /uploads

add . /app
workdir /app
run pip install . /app
