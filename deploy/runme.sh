#!/bin/bash

if [ ! -e $SSH_KEY ]; then
	echo "ssh key is not found"
	exit 1
fi

echo "Making sure that we are up todate"
sudo apt-get update

echo "Making sure that we have python et al."
sudo apt-get install -y python-dev python-pip

echo "Updating pip"
sudo pip install -U pip

echo "Installing ansible"
sudo pip install ansible

echo "Installing boto"
sudo pip install boto3 boto

echo "Configuring boto"
if [ ! -f ~/.boto ]; then
tee ~/.boto << EOF
[Credentials]
aws_access_key_id=${AWS_ACCESS_KEY}
aws_secret_access_key=${AWS_SECRET_KEY}
EOF
fi

echo "Configuring ansible"
if [ ! -f $(pwd)/ansible.cfg ]; then
tee $(pwd)/ansible.cfg <<EOF
[defaults]
inventory = inventory
remote_user = ubuntu
host_key_checking = False
private_key_file = ${SSH_KEY}
EOF
fi

echo "Deploying aws instances and configuring software."
if [ ! -f config.yaml ]; then
cp config.yaml.sample config.yaml
fi
ansible-playbook -e @config.yaml aws-launch.yaml
./inventory/ec2.py --refresh-cache
ansible-playbook -e @config.yaml site.yaml
