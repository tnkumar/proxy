Configuring ansible with boto
-----------------------------

Note: Please make sure that your ssh key is available in order to script to run.

1. Configure your AWS EC2 keys as the following:

	export AWS_ACCESS_KEY="<your access key>"
	export AWS_SECRET_KEY="<your secret key>"

2. Configure your SSH key accordingly: 

	export SSH_KEY="<path to your ssh key>"

3. Modify the config.yaml.sample for your specific EC2 Account.

4. run the script:

./runme.sh

Running playbooks individually
------------------------------

Provided the environment variables and the config.yaml are set up correctly:

To deploy the application

ansible-playbook -e @config.yaml site.yaml

To launch aws instances:

ansible-playbookt -e @config.yaml aws-launch.yaml
