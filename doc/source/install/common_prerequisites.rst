Prerequisites
-------------

Before you install and configure the proxy service,
you must create a database, service credentials, and API endpoints.

#. To create the database, complete these steps:

   * Use the database access client to connect to the database
     server as the ``root`` user:

     .. code-block:: console

        $ mysql -u root -p

   * Create the ``proxy`` database:

     .. code-block:: none

        CREATE DATABASE proxy;

   * Grant proper access to the ``proxy`` database:

     .. code-block:: none

        GRANT ALL PRIVILEGES ON proxy.* TO 'proxy'@'localhost' \
          IDENTIFIED BY 'PROXY_DBPASS';
        GRANT ALL PRIVILEGES ON proxy.* TO 'proxy'@'%' \
          IDENTIFIED BY 'PROXY_DBPASS';

     Replace ``PROXY_DBPASS`` with a suitable password.

   * Exit the database access client.

     .. code-block:: none

        exit;

#. Source the ``admin`` credentials to gain access to
   admin-only CLI commands:

   .. code-block:: console

      $ . admin-openrc

#. To create the service credentials, complete these steps:

   * Create the ``proxy`` user:

     .. code-block:: console

        $ openstack user create --domain default --password-prompt proxy

   * Add the ``admin`` role to the ``proxy`` user:

     .. code-block:: console

        $ openstack role add --project service --user proxy admin

   * Create the proxy service entities:

     .. code-block:: console

        $ openstack service create --name proxy --description "proxy" proxy

#. Create the proxy service API endpoints:

   .. code-block:: console

      $ openstack endpoint create --region RegionOne \
        proxy public http://controller:XXXX/vY/%\(tenant_id\)s
      $ openstack endpoint create --region RegionOne \
        proxy internal http://controller:XXXX/vY/%\(tenant_id\)s
      $ openstack endpoint create --region RegionOne \
        proxy admin http://controller:XXXX/vY/%\(tenant_id\)s
