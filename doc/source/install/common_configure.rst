2. Edit the ``/etc/proxy/proxy.conf`` file and complete the following
   actions:

   * In the ``[database]`` section, configure database access:

     .. code-block:: ini

        [database]
        ...
        connection = mysql+pymysql://proxy:PROXY_DBPASS@controller/proxy
