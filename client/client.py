#!/usr/bin/python

import hashlib
import time
import csv
import json
import requests
import sys
import base64
import os
import os.path
from uuid import getnode as get_mac
from cStringIO import StringIO

url = 'http://0.0.0.0/v1/device'

def getMacAddress():
    mac = get_mac()
    return hex(mac)

def getDeviceId():
    name = 'PI' + str(getMacAddress())
    return name

def sha256_checksum_string(inputString, block_size=65536):
    # bytearray.fromhex(inputString)
    sha256 = hashlib.sha256(inputString).hexdigest()
    return sha256


def sha256_checksum(filename, block_size=65536):
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()


def sha256_checksum_dir(dir):
    buf = StringIO()
    for path, subdirs, files in os.walk(dir):
        for filename in files:
            pf = os.path.join(path, filename)
            checksum = sha256_checksum(pf)
            buf.write(checksum)
            print('hash of ' + pf + '  :' + checksum)
    print buf.getvalue()
    mainHash = sha256_checksum_string(buf.getvalue())
    return mainHash


def main():
    mainBuf = StringIO()
    for f in sys.argv[1:]:
        mainHash = sha256_checksum_dir(f)
        mainBuf.write(mainHash)
    devId = getDeviceId()
    mainBuf.write(sha256_checksum_string(devId))
    globalHash = sha256_checksum_string(mainBuf.getvalue())
    checksum = globalHash

    f = open('data.list', 'rb')
    render = csv.DictReader(f)
    rownum = 0
    for row in render:
        result = {}
        result['deviceId'] = row['name']
        result['devicePassword'] = row['devicePassword']
        result['checksum'] = checksum
        result['ip'] = row['ip']
        result['version'] = row['version']
        resp  = requests.post(url, json=result)
        print resp
if __name__ == '__main__':
    main()
